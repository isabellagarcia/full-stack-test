import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NbSecurityModule } from '@nebular/security';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Services
import { DataService } from './services/data.service';


@NgModule({
	imports: [
		CommonModule,
	],
	exports: [
		CommonModule,
		BrowserAnimationsModule,
	],
	providers: [
		NbSecurityModule.forRoot({
			accessControl: {
				guest: {
					view: '*',
				},
				user: {
					parent: 'guest',
					create: '*',
					edit: '*',
					remove: '*',
				},
			},
		}).providers,
		DatePipe,
		DataService
	],
	declarations: [],
})

export class CoreModule {}
