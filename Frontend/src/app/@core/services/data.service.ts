import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {}

  public addProduct(data: any) {
    const url = `${environment.api}/addProduct`;
    return this.http.post(url, data);
  }

  public products() {
    const url = `${environment.api}/products`;
    return this.http.get(url);
  }

  public openCart() {
    const url = `${environment.api}/cart`;
    return this.http.get(url);
  }

  public removeProduct(data) {
    const url = `${environment.api}/removeProduct`;
    return this.http.post(url, data);
  }

}
