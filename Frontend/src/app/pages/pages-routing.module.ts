import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { ListComponent } from './list/list.component';
import { CartComponent } from './cart/cart.component';


const routes: Routes = [{
	path: '',
	component: PagesComponent,
	children: [
		{
			path: 'list',
			component: ListComponent
		},
		{
			path: 'cart',
			component: CartComponent
		},
		{
			path: '',
			redirectTo: 'list',
			pathMatch: 'full',
		}
	],
}];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class PagesRoutingModule {}
