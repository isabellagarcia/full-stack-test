import { Component, OnInit } from '@angular/core';
import { DataService } from '../../@core/services/data.service';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public products: any;

  constructor(private service: DataService, private toastrService: NbToastrService, private router: Router) { }

  ngOnInit() {
    this.service.products().subscribe(result =>{
      console.log(result);
      this.products = result['data'];
    })
  }

  public addProduct(id){
    this.service.addProduct({sku: id, quantity: 1}).subscribe(result => {
        this.toastrService.success(result, '', {icon: ''});
      
    }, err =>{
      this.toastrService.danger('Produto sem estoque suficiente!','', {icon: ''});
    });

  }

  public openCart(){
    this.router.navigate(['/sallve/cart']);
  }

}
