import { Component, OnInit } from '@angular/core';
import { DataService } from '../../@core/services/data.service';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  public cart: any;
  public total: any;
  public quantity_sku: any;
  constructor(private service: DataService, private router: Router,  private toastrService: NbToastrService) { }

  ngOnInit() {
    this.openCart();
  }

  public openCart(){
    this.service.openCart().subscribe(result =>{
      this.cart = result['cart'];
      this.total = result['total'];
      this.quantity_sku = result['quantity_sku'];
      console.log(result);
    });
  }

  public back(){
    this.router.navigate(['/sallve/list']);
  }

  public addProduct(id, quantity, quantityItem){
    if((quantity + quantityItem) == 0){
      this.service.removeProduct({sku: id}).subscribe(result => {
        this.toastrService.success(result);
        this.openCart();
      })
    }else{
      this.service.addProduct({sku:id, quantity: quantity}).subscribe(result => {
        console.log(result);
        this.openCart();
      }, err =>{
        this.toastrService.danger('Produto sem estoque suficiente!','', {icon: ''});
      })
    }

    
  }

}
