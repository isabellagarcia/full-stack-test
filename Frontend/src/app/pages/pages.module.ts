import { NgModule } from '@angular/core';

import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';

import { PagesComponent } from './pages.component';
import { ListComponent } from './list/list.component';
import { CartComponent } from './cart/cart.component';


@NgModule({
	imports: [
		PagesRoutingModule,
		ThemeModule,
	],
	declarations: [
		PagesComponent,
		ListComponent,
		CartComponent,
	],
})



export class PagesModule {}
