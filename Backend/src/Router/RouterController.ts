import express from 'express';
import RouterResponse from './RouterResponse';
import Controller from '../Controllers/CartController';
import CartController from '../Controllers/CartController';

export default class RouterController{

    public routes: express.Router;

    constructor(){
        this.routes = express.Router();
        this.middlewares();
    }

    public middlewares(){

        this.loadRoutes();

        this.routes.use('*', (req: express.Request, res: express.Response, next: express.NextFunction) => RouterResponse.notFound(res));

        this.routes.use((error: Error, req: express.Request, res: express.Response, next: express.NextFunction) => RouterResponse.serverError(error, res));
    }

    private loadRoutes(){
        this.routes.get('/products', (req, res) => CartController.getProducts(req, res));
        this.routes.get('/cart', (req, res) => CartController.getCart(req, res));
        this.routes.post('/addProduct', (req, res) => CartController.addProduct(req, res));
        this.routes.post('/removeProduct', (req, res) => CartController.removeProduct(req, res));
    }
}