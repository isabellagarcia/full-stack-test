import express from 'express';
import RouterResponse from '../Router/RouterResponse';
import * as firebaseAdmin from "firebase-admin";


export default class CartController {
    
    public static cart: Array<any> = [];

    /**
     * getCart
     *
     * Retorna todas as informações do carrinho de compras
     *
     * @public
     * @author Isabella Garcia
     * @since  12/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static getCart(req: express.Request, res: express.Response){

        if(this.cart.length == 0){
            RouterResponse.success({cart: this.cart}, res);
        }else{
            let total = 0;
            this.cart.forEach(item => {
                total = total + item.amount;
            });
            let data = {
                cart: this.cart,
                total: total,
                quantity_sku: this.cart.length
            };

            RouterResponse.success(data, res);

        }
        
    }

    /**
     * getProducts
     *
     * Retorna todos os produtos
     *
     * @public
     * @author Isabella Garcia
     * @since  12/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static getProducts(req: express.Request, res: express.Response){

        const db = firebaseAdmin.firestore();
        var products: Array<any> = []; 

        db.collection('product').get().then((snapshot) => {
            snapshot.forEach(item => {
                products.push(item.data());
            })

            RouterResponse.success({data: products}, res);
        }).catch(err => {
            RouterResponse.error('Houve um erro ao buscar os produtos', res);
        });
        
    }

    /**
     * addProduct
     *
     * Adiciona ou atualiza um produto do carrinho
     *
     * @public
     * @author Isabella Garcia
     * @since  12/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static  addProduct(req: express.Request, res: express.Response){
        const db = firebaseAdmin.firestore();
        const data = req.body;
        let exist: boolean = false;

        db.collection('product').doc(data.sku).get().then((document) => {
            let product = document.data();

            if(product.inventory <= 0){
                RouterResponse.error('Produto sem estoque suficiente!', res);
            }else{
                this.cart.forEach(item => {
                    if(item.sku == data.sku){
                        if(product.inventory >= (item.quantity + data.quantity)){
                            item.quantity = item.quantity + data.quantity;
                            item.amount = item.unitary * item.quantity; 

                            RouterResponse.success('Produto adicionado com sucesso.', res);
                        }else{
                            RouterResponse.error('Produto sem estoque suficiente!', res);
                        }
                        exist = true;
                    }
                });
                if(!exist){
                    if(product.inventory >= data.quantity){
                        this.cart.push({
                            sku: data.sku,
                            quantity: data.quantity,
                            unitary: product.price,
                            amount: product.price * data.quantity,
                            name: product.name,
                            image: product.image
                        });
        
                        RouterResponse.success('Produto adicionado com sucesso.', res);
                    }else{
                        RouterResponse.error('Produto sem estoque suficiente!', res);
                    }
                } 
            }
           
        }).catch((err) => {
            RouterResponse.error('Houve um erro ao atualizar carrinho', res);
        });
        
    }

    /**
     * removeProduct
     *
     * Retira um produto do carrinho
     *
     * @public
     * @author Isabella Garcia
     * @since  12/2020
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static removeProduct(req: express.Request, res: express.Response){
        const sku = req.body.sku;

        this.cart.forEach((item, index) => {
            if(item.sku == sku){
                this.cart.splice(index, 1);
            }
        });
        
        RouterResponse.success('Produto removido com sucesso.', res);
        
    }



}