import express from 'express';
import http from 'http';
import RouterController from './Router/RouterController';
import bodyParser from 'body-parser';
import compression from 'compression';
import * as firebaseAdmin from "firebase-admin";
import cors from 'cors';




const app: express.Express = express();
const server: http.Server = http.createServer(app);
const port: number = 4000;
const router: RouterController = new RouterController;
//options for cors midddleware
const options: cors.CorsOptions = {
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'X-Access-Token',
  ],
  credentials: true,
  methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
  origin: 'localhost:4200',
  preflightContinue: false,
};

//use cors middleware
app.use(cors({
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'X-Access-Token',
  ],
  credentials: false,
  methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
  origin: 'http://localhost:4200',
  preflightContinue: false,
}));
app.use(bodyParser.json()); // converte body para objeto
app.use(compression()); // compressão de GZip 
app.use(router.routes); // importa rotas do express


firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert('./sallve-firebase.json'),
    databaseURL: 'https://sallve-85185.firebaseio.com'
  });

server.listen(port, () => {
    console.log('Server start');
})
