# Projeto Sallve #

Como rodar localmente os projetos. Primeiramente clone o projeto.

### API ###

* É necessário ter o NodeJS instalado
* Na pasta 'Backend', rodar o comando 'npm install' e depois 'npm run build'


### Site ###

* É necessário ter o Angular instalado
* Na pasta 'Frontend' rodar o comando 'npm install' e depois 'ng serve'.
* Abrir no navegador, a url 'localhost:4200'.